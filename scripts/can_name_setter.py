#!/usr/bin/env python

import rospy
import os

stream = os.popen("dmesg | grep -oP \"(peak_usb(.*)can)\d+\" | awk '{print $NF}'")
list_can_interfaces = stream.read().splitlines()

list_params = [
  "/srr_01/umrr_can_publisher/can_socket",
  "/srr_02/umrr_can_publisher/can_socket",
  "/lrr_01/umrr_can_publisher/can_socket",
  "/srr_03/umrr_can_publisher/can_socket",
  "/srr_04/umrr_can_publisher/can_socket",
  "/lrr_02/umrr_can_publisher/can_socket"
]

assert len(list_params) == len(list_can_interfaces)

for i in range(len(list_params)):
  rospy.set_param(list_params[i], list_can_interfaces[i])
  print("set param: " + list_params[i] + " = " + list_can_interfaces[i])

