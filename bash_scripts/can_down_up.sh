#!/usr/bin/env bash

echo "Can downs and ups starting..."


sudo ip link set can0 down
sudo ip link set can0 type can bitrate 500000
sudo ip link set can0 up

sudo ip link set can1 down
sudo ip link set can1 type can bitrate 500000
sudo ip link set can1 up

sudo ip link set can2 down
sudo ip link set can2 type can bitrate 500000
sudo ip link set can2 up

sudo ip link set can3 down
sudo ip link set can3 type can bitrate 500000
sudo ip link set can3 up

sudo ip link set can4 down
sudo ip link set can4 type can bitrate 500000
sudo ip link set can4 up

sudo ip link set can5 down
sudo ip link set can5 type can bitrate 500000
sudo ip link set can5 up

sudo ip link set can6 down
sudo ip link set can6 type can bitrate 500000
sudo ip link set can6 up

sudo ip link set can7 down
sudo ip link set can7 type can bitrate 500000
sudo ip link set can7 up

echo "Can downs and ups ended."